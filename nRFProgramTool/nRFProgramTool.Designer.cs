﻿namespace nRFProgramTool
{
    partial class Form_nRFTool
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_fileName = new System.Windows.Forms.TextBox();
            this.button_openFile = new System.Windows.Forms.Button();
            this.button_eraseall = new System.Windows.Forms.Button();
            this.textBox_outLog = new System.Windows.Forms.TextBox();
            this.button_program = new System.Windows.Forms.Button();
            this.button_protec = new System.Windows.Forms.Button();
            this.textBox_deviceID = new System.Windows.Forms.TextBox();
            this.checkBox_aotu = new System.Windows.Forms.CheckBox();
            this.label_autoResult = new System.Windows.Forms.Label();
            this.label_sucessNumico = new System.Windows.Forms.Label();
            this.label_sucessNum = new System.Windows.Forms.Label();
            this.textBox_clockspeed = new System.Windows.Forms.TextBox();
            this.button_clockspeed = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox_fileName
            // 
            this.textBox_fileName.Location = new System.Drawing.Point(43, 32);
            this.textBox_fileName.Name = "textBox_fileName";
            this.textBox_fileName.Size = new System.Drawing.Size(131, 21);
            this.textBox_fileName.TabIndex = 0;
            // 
            // button_openFile
            // 
            this.button_openFile.Location = new System.Drawing.Point(180, 32);
            this.button_openFile.Name = "button_openFile";
            this.button_openFile.Size = new System.Drawing.Size(75, 23);
            this.button_openFile.TabIndex = 1;
            this.button_openFile.Text = "打开文件";
            this.button_openFile.UseVisualStyleBackColor = true;
            this.button_openFile.Click += new System.EventHandler(this.button_openFile_Click);
            // 
            // button_eraseall
            // 
            this.button_eraseall.Location = new System.Drawing.Point(43, 114);
            this.button_eraseall.Name = "button_eraseall";
            this.button_eraseall.Size = new System.Drawing.Size(75, 23);
            this.button_eraseall.TabIndex = 2;
            this.button_eraseall.Text = "擦除";
            this.button_eraseall.UseVisualStyleBackColor = true;
            this.button_eraseall.Click += new System.EventHandler(this.button_eraseall_Click);
            // 
            // textBox_outLog
            // 
            this.textBox_outLog.Location = new System.Drawing.Point(43, 149);
            this.textBox_outLog.Multiline = true;
            this.textBox_outLog.Name = "textBox_outLog";
            this.textBox_outLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_outLog.Size = new System.Drawing.Size(212, 81);
            this.textBox_outLog.TabIndex = 3;
            // 
            // button_program
            // 
            this.button_program.Location = new System.Drawing.Point(124, 114);
            this.button_program.Name = "button_program";
            this.button_program.Size = new System.Drawing.Size(75, 23);
            this.button_program.TabIndex = 4;
            this.button_program.Text = "编程";
            this.button_program.UseVisualStyleBackColor = true;
            this.button_program.Click += new System.EventHandler(this.button_program_Click);
            // 
            // button_protec
            // 
            this.button_protec.Location = new System.Drawing.Point(205, 114);
            this.button_protec.Name = "button_protec";
            this.button_protec.Size = new System.Drawing.Size(75, 23);
            this.button_protec.TabIndex = 5;
            this.button_protec.Text = "保护";
            this.button_protec.UseVisualStyleBackColor = true;
            this.button_protec.Click += new System.EventHandler(this.button_protec_Click);
            // 
            // textBox_deviceID
            // 
            this.textBox_deviceID.Enabled = false;
            this.textBox_deviceID.Location = new System.Drawing.Point(43, 60);
            this.textBox_deviceID.Name = "textBox_deviceID";
            this.textBox_deviceID.Size = new System.Drawing.Size(131, 21);
            this.textBox_deviceID.TabIndex = 6;
            // 
            // checkBox_aotu
            // 
            this.checkBox_aotu.AutoSize = true;
            this.checkBox_aotu.Location = new System.Drawing.Point(180, 64);
            this.checkBox_aotu.Name = "checkBox_aotu";
            this.checkBox_aotu.Size = new System.Drawing.Size(48, 16);
            this.checkBox_aotu.TabIndex = 7;
            this.checkBox_aotu.Text = "自动";
            this.checkBox_aotu.UseVisualStyleBackColor = true;
            this.checkBox_aotu.CheckedChanged += new System.EventHandler(this.checkBox_aotu_CheckedChanged);
            // 
            // label_autoResult
            // 
            this.label_autoResult.AutoSize = true;
            this.label_autoResult.Location = new System.Drawing.Point(43, 249);
            this.label_autoResult.Name = "label_autoResult";
            this.label_autoResult.Size = new System.Drawing.Size(0, 12);
            this.label_autoResult.TabIndex = 8;
            // 
            // label_sucessNumico
            // 
            this.label_sucessNumico.AutoSize = true;
            this.label_sucessNumico.Location = new System.Drawing.Point(206, 248);
            this.label_sucessNumico.Name = "label_sucessNumico";
            this.label_sucessNumico.Size = new System.Drawing.Size(41, 12);
            this.label_sucessNumico.TabIndex = 9;
            this.label_sucessNumico.Text = "成功：";
            // 
            // label_sucessNum
            // 
            this.label_sucessNum.AutoSize = true;
            this.label_sucessNum.Location = new System.Drawing.Point(253, 248);
            this.label_sucessNum.Name = "label_sucessNum";
            this.label_sucessNum.Size = new System.Drawing.Size(11, 12);
            this.label_sucessNum.TabIndex = 10;
            this.label_sucessNum.Text = "0";
            // 
            // textBox_clockspeed
            // 
            this.textBox_clockspeed.Location = new System.Drawing.Point(43, 87);
            this.textBox_clockspeed.Name = "textBox_clockspeed";
            this.textBox_clockspeed.Size = new System.Drawing.Size(100, 21);
            this.textBox_clockspeed.TabIndex = 11;
            this.textBox_clockspeed.Text = "500";
            // 
            // button_clockspeed
            // 
            this.button_clockspeed.Location = new System.Drawing.Point(180, 85);
            this.button_clockspeed.Name = "button_clockspeed";
            this.button_clockspeed.Size = new System.Drawing.Size(75, 23);
            this.button_clockspeed.TabIndex = 12;
            this.button_clockspeed.Text = "设置速度";
            this.button_clockspeed.UseVisualStyleBackColor = true;
            this.button_clockspeed.Click += new System.EventHandler(this.button_clockspeed_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "V1.0.1";
            // 
            // Form_nRFTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 273);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_clockspeed);
            this.Controls.Add(this.textBox_clockspeed);
            this.Controls.Add(this.label_sucessNum);
            this.Controls.Add(this.label_sucessNumico);
            this.Controls.Add(this.label_autoResult);
            this.Controls.Add(this.checkBox_aotu);
            this.Controls.Add(this.textBox_deviceID);
            this.Controls.Add(this.button_protec);
            this.Controls.Add(this.button_program);
            this.Controls.Add(this.textBox_outLog);
            this.Controls.Add(this.button_eraseall);
            this.Controls.Add(this.button_openFile);
            this.Controls.Add(this.textBox_fileName);
            this.Name = "Form_nRFTool";
            this.Text = "nRFProgramTool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_nRFTool_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_fileName;
        private System.Windows.Forms.Button button_openFile;
        private System.Windows.Forms.Button button_eraseall;
        private System.Windows.Forms.TextBox textBox_outLog;
        private System.Windows.Forms.Button button_program;
        private System.Windows.Forms.Button button_protec;
        private System.Windows.Forms.TextBox textBox_deviceID;
        private System.Windows.Forms.CheckBox checkBox_aotu;
        private System.Windows.Forms.Label label_autoResult;
        private System.Windows.Forms.Label label_sucessNumico;
        private System.Windows.Forms.Label label_sucessNum;
        private System.Windows.Forms.TextBox textBox_clockspeed;
        private System.Windows.Forms.Button button_clockspeed;
        private System.Windows.Forms.Label label1;
    }
}

